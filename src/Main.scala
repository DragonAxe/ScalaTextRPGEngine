object Main {
  def main(args: Array[String]) = {
    println("Welcome to the functional scala text RPG engine!")

    SimpleGame.gameLoop(EnchantiaGameState)
  }
}
