object AreaForestSouth extends Area {
  override val name = "The Forest"
  override val description = "You stand in the middle of a forest. The trees arch up above your head. There are paths through the underbrush to the north and south."
  override val navigations = new Navigation(AreaObelisk, "go to the south forest") :: List()
}
