abstract class Area {
  val name: String
  val description: String
  val navigations: List[Navigation]

  def getName = name
  def getDescription = description
}
