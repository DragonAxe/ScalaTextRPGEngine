object AreaObelisk extends Area {
  override val name = "The Obelisk"
  override val description = "You stand near an obelisk with no memory of how you got there. The sun is going down and the wind is whipping at your clothes. There are four paths leading into the forest all around you, one for each compas direction."
  override val navigations = new Navigation(AreaForestSouth, "go to the obelisk") :: List()
}
